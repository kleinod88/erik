/*
 * Erik Framework
 * Copyright (C) 2022  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef DIALOG_H
#define DIALOG_H

#include <raylib.h>
#include <string>
#include <vector>

namespace Erik_FW{
/**
 * @todo write docs
 */

std::vector<std::string> split_string_by_newline(const std::string& str);

class Dialog
{
public:
    static Dialog* Instance();

    void SetMSG (std::string msg);

    void Update();

    bool Exist();

    void DrawDialog ();

private:

    static Dialog *_Instance;
    bool _MSGAvaible;
    std::vector<std::string> _MSG;
    Font _Font;
    int _LinePos;
    Dialog();
};
};//namespace Erik_FW
#endif // DIALOG_H
