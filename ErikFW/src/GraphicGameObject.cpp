/*
 * Erik Framework
 * Copyright (C) 2022   Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "GraphicGameObject.hpp"
#include "TextureManager.hpp"
#include "Game.hpp"
#include "Log.hpp"
#include "Settings.hpp"

Erik_FW::GraphicGameObject::GraphicGameObject()
    :GameObject(), _Position{0,0},_Velocity{0,0}, _Acceleration{0,0} {
}



void Erik_FW::GraphicGameObject::Load(const std::string script){
}


void Erik_FW::GraphicGameObject::Draw()
{
    //Es Wird nur angezeigt, wenn die lebensenergie größer 0 ist
    if (!_Visable)
         return;
    PLOGV << "Draw Texture : " << _TextureID;
    PLOGV << "     Position: " << _Position.x << " x " << _Position.y;
    PLOGV << "     Breite  : " << _Width;
    PLOGV << "     Höhe    : " << _Height;
    PLOGV << "     Spalte  : " << _CurrentFrame;
    PLOGV << "     Zeile   : " << _CurrentRow;
    TextureManager::Instance()->DrawFrame(_TextureID, (int)_Position.x, (int)_Position.y, _Width, _Height, _CurrentFrame, _CurrentRow);
    if (Settings::Instance()->GetCollisionBoxes()) {
        DrawRectangleLines(_ObjectCollision.x, _ObjectCollision.y, _ObjectCollision.width, _ObjectCollision.height, RED );
        DrawRectangleLines(_NorthCollision.x, _NorthCollision.y, _NorthCollision.width, _NorthCollision.height, BLUE );
        DrawRectangleLines(_EastCollision.x, _EastCollision.y, _EastCollision.width, _EastCollision.height, BLUE );
        DrawRectangleLines(_SouthCollision.x, _SouthCollision.y, _SouthCollision.width, _SouthCollision.height, BLUE );
        DrawRectangleLines(_WestCollision.x, _WestCollision.y, _WestCollision.width, _WestCollision.height, BLUE );
    }
}

void Erik_FW::GraphicGameObject::Clean()
{
}

void Erik_FW::GraphicGameObject::Update()
{
    if (_Visable)
        return;
}

Vector2  Erik_FW::GraphicGameObject::GetPosition()
{
    return _Position;
}

int Erik_FW::GraphicGameObject::GetWidth()
{
    return _Width;
}


int Erik_FW::GraphicGameObject::GetHeight()
{
    return _Height;
}

void Erik_FW::GraphicGameObject::SetName(std::string name) {
    _Name = name;
}

std::string Erik_FW::GraphicGameObject::GetName() {
    return _Name;
}

void Erik_FW::GraphicGameObject::SetActivCollision(int activ) {
    if (activ >= 0 && activ <=4) {
        _ActivCollision = activ;
    }
}

int Erik_FW::GraphicGameObject::GetActivCollision() {
    return _ActivCollision;
}

void Erik_FW::GraphicGameObject::SetObject(Rectangle object) {
    _ObjectCollision = object;
}

Rectangle Erik_FW::GraphicGameObject::GetObject() {
    return _ObjectCollision;
}

void Erik_FW::GraphicGameObject::SetNorth(Rectangle north) {
    _NorthCollision = north;
}

Rectangle Erik_FW::GraphicGameObject::GetNorth() {
    return _NorthCollision;
}

void Erik_FW::GraphicGameObject::SetEast(Rectangle east) {
    _EastCollision = east;
}

Rectangle Erik_FW::GraphicGameObject::GetEast() {
    return _EastCollision;
}

void Erik_FW::GraphicGameObject::SetSouth(Rectangle south) {
    _SouthCollision = south;
}

Rectangle Erik_FW::GraphicGameObject::GetSouth() {
    return _SouthCollision;
}

void Erik_FW::GraphicGameObject::SetWest(Rectangle west) {
    _WestCollision = west;
}

Rectangle Erik_FW::GraphicGameObject::GetWest() {
    return _WestCollision;
}

bool Erik_FW::GraphicGameObject::CollisionDetect([[maybe_unused]] GraphicGameObject* obj) {
    return false;
}

void Erik_FW::GraphicGameObject::SetPosition(float x, float y){
    _Position.x = x;
    _Position.y = y;
}

void Erik_FW::GraphicGameObject::SetHeight(float h){
    _Height = h;
}

void Erik_FW::GraphicGameObject::SetWidth(float w){
    _Width = w;
}

