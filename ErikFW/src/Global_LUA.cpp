/*
 * Erik Framework
 * Copyright (C) 2022   Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Global_LUA.hpp"
#include "Game.hpp"
#include "Settings.hpp"
#include "GameStateMaschine.hpp"
#include "StoryNode.hpp"
#include "MapManager.hpp"

void Erik_FW::LuaSetup (sol::state *state){
    state->new_usertype<Settings>("Settings",
        "new", sol::no_constructor,
        "Instance", &Settings::Instance,
        "GetFullscreen", &Settings::GetFullScreen,
        "GetWindowWidth", &Settings::GetWindowWidth,
        "GetWindowHeight", &Settings::GetWindowHeight,
        "GetFSHeight", &Settings::GetFSHeight,
        "GetFSWidth", &Settings::GetFSWidth,
        "GetFPS", &Settings::GetFPS,
        "GetCollisionBoxes", &Settings::GetCollisionBoxes,
        "GetMusicVolume", &Settings::GetMusicVolume,
        "GetEffectVolume", &Settings::GetEffectVolume,
        "SetFullscreen", &Settings::SetFullscreen,
        "SetWindowWidth", &Settings::SetWindowWidth,
        "SetWindowHeight", &Settings::SetWindowHeight,
        "SetFPS", &Settings::SetFPS,
        "SetCollisionBoxes", &Settings::SetCollisionBoxes,
        "SetMusicVolume", &Settings::SetMusicVolume,
        "SetEffectVolume", &Settings::SetEffectVolume,
        "Save", &Settings::Save
        );

    state->new_usertype<Game>("Game",
        "new", sol::no_constructor,
        "Instance", &Game::Instance,
        "IsFS", &Game::IsFS,
        "ToggleFullscreen", &Game::ToggleFS,
        "Quit", &Game::Quit
        );



    state->new_usertype<GameStateMaschine>("StateMaschine",
        "new", sol::no_constructor,
        "Instance", &GameStateMaschine::Instance,
        "PushState", &GameStateMaschine::PushState,
        "ChangeState", &GameStateMaschine::ChangeState,
        "PopState", &GameStateMaschine::PopState
        );
    state->new_usertype<StoryNode>("StoryNode",
        "new", sol::no_constructor,
        "Instance", &StoryNode::Instance,
        "NewNode", &StoryNode::NewNode,
        "IsFinished", &StoryNode::NodeFinished
        );



    state->new_usertype<MapManager>("MapManager",
        "new", sol::no_constructor,
        "Instance", &MapManager::Instance,
        "ChanceToMap", &MapManager::ChangeCurrentMap
        );

}
