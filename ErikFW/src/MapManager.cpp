/*
 * Erik Framework
 * Copyright (C) 2022   Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MapManager.hpp"
#include "MapParser.hpp"
#include <raylib.h>
#include <string>
#include "Log.hpp"

Erik_FW::MapManager* Erik_FW::MapManager::_Instance = 0;

bool Erik_FW::MapManager::LoadMap(std::string fileName, std::string id) {
    //Prüfen ob es schon eine Karte mit der id gibt
    if (Exist(id)){
        PLOGW << "Karte mit ID "  << id << " existiert bereits";
        return false;
    }

    //Karte aus Datei laden
    MapParser parser;
    Map *newMap = parser.ParseMap(fileName);

    //Prüfen ob Karte geladen wurde
    if (newMap == nullptr){
        PLOGE << "Karte " << id << " konnte nicht geladen werden";
        return false;
    }

    //Karte zum Stapel hinzufügen
    _Maps[id] = newMap;
    PLOGI << "Karte erfolgreich geladen: " << id;
    return true;
}

void Erik_FW::MapManager::Draw () {
    _CurrentMap->Render();
}

bool Erik_FW::MapManager::ChangeCurrentMap(std::string id){
    if (Exist(id)){
        _CurrentMap = _Maps[id];
        return true;
    }
    PLOGW << "Konnte nicht zur Karte: " << id << " wchseln: nicht vorhanden";
    return false;
}

bool Erik_FW::MapManager::RemoveMap(std::string id){
    return false;
}

void Erik_FW::MapManager::Update(){
    _CurrentMap->Update();
}

Erik_FW::MapManager * Erik_FW::MapManager::Instance(){
    if (_Instance == nullptr){
        _Instance = new MapManager();
    }
    return _Instance;
}

bool Erik_FW::MapManager::Collision(GameObject *object){
    return _CurrentMap->Collison(object);
}

int Erik_FW::MapManager::GetWidth(){
    return _CurrentMap->GetWidth();
}

int Erik_FW::MapManager::GetHeight(){
    return _CurrentMap->GetHeight();
}


Erik_FW::MapManager::MapManager() {

}

Erik_FW::MapManager::~MapManager()
{
}

bool Erik_FW::MapManager::Exist(std::string id) {
    if (_Maps.count(id) == 0)
        return false;
    else
        return true;
}

Erik_FW::Layer * Erik_FW::MapManager::GetObjectLayer(){
    return _CurrentMap->GetObjectLayer();
}

