/*
 * Erik Framework
 * Copyright (C) 2022  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <fstream>
#include <filesystem>
#define SOL_ALL_SAFETIES_ON 1
#include "../../Extra/sol/sol.hpp"
#include <raylib.h>
#include "Settings.hpp"
#include "Log.hpp"
#include <Config.hpp>

Erik_FW::Settings* Erik_FW::Settings::_Instance = nullptr;


Erik_FW::Settings* Erik_FW::Settings::Instance() {
    if (_Instance == nullptr)
        _Instance = new Settings();
    return _Instance;
}

Erik_FW::Settings::Settings()
    : _Fullscreen(true), _CollisionBoxes(false), _WindowedWidth(800),
      _WindowedHeight(600), _FPS(60), _MusicVolume(0.5f), _EffectVolume(0.5f)
{
}

bool Erik_FW::Settings::Load() {
    sol::state lua;
    lua.open_libraries(sol::lib::base);
    lua.open_libraries(sol::lib::string);

    std::string confFile = std::string (getenv("HOME")) + std::string ("/.Erik/Settings.lua");

    if (!FileExists(confFile.c_str())){
            Save();
    }

    lua.script_file (confFile);
    _Fullscreen = lua.get<bool>("FULLSCREEN");
    _CollisionBoxes = lua.get<bool>("COLLISIONBOXES");
    _WindowedWidth =  lua.get<int>("WINDOWWIDTH");
    _WindowedHeight =  lua.get<int>("WINDOWHEIGHT");
    _MusicVolume =  lua.get<float>("MUSICVOLUME");
    _EffectVolume =  lua.get<float>("EFECTVOLUME");

    return true;
}

void Erik_FW::Settings::Save(){
    std::ofstream file;
    std::string confFile = std::string (getenv("HOME")) + std::string ("/.Erik/Settings.lua");
    std::string erikDir = std::string (getenv("HOME")) + std::string ("/.Erik");

    if (!FileExists(confFile.c_str())){
            if (!DirectoryExists(erikDir.c_str())){
                PLOGI << "Erstelle Verzeichnis: " << erikDir;
                std::filesystem::create_directory(erikDir);
            }
    }
    file.open (confFile);
    file << "FULLSCREEN = ";
    if (_Fullscreen)
        file << "true\n";
    else
        file << "false\n";
    file << "COLLISIONBOXES = ";
    if (_CollisionBoxes)
        file << "true\n";
    else
        file << "false\n";
    file << "WINDOWWIDTH = " << _WindowedWidth << "\n";
    file << "WINDOWHEIGHT = " << _WindowedHeight << "\n";
    file << "MUSICVOLUME = " << _MusicVolume << "\n";
    file << "EFECTVOLUME = " << _EffectVolume << "\n";
    PLOGI << "Settings in " << confFile << " gespeichert!";
    file.close();
}


void Erik_FW::Settings::SetFullscreen(bool fullscreen) {
    _Fullscreen = fullscreen;
}

void Erik_FW::Settings::SetCollisionBoxes(bool boxes) {
    _CollisionBoxes = boxes;
}



void Erik_FW::Settings::SetWindowWidth(int width) {
    _WindowedWidth = width;
}

void Erik_FW::Settings::SetWindowHeight(int height) {
    _WindowedHeight = height;
}

void Erik_FW::Settings::SetFPS(int fps){
    _FPS = fps;
}

void Erik_FW::Settings::SetMusicVolume(float volume) {
    _MusicVolume = volume;
}

void Erik_FW::Settings::SetEffectVolume(float volume) {
    _EffectVolume = volume;
}

bool Erik_FW::Settings::GetFullScreen() {
    return _Fullscreen;
}

bool Erik_FW::Settings::GetCollisionBoxes() {
    return _CollisionBoxes;
}

int Erik_FW::Settings::GetWindowWidth() {
    return _WindowedWidth;
}

int Erik_FW::Settings::GetWindowHeight() {
    return _WindowedHeight;
}

int Erik_FW::Settings::GetFPS(){
    return _FPS;
}
float Erik_FW::Settings::GetMusicVolume() {
    return _MusicVolume;
}

float Erik_FW::Settings::GetEffectVolume() {
    return _EffectVolume;
}

int Erik_FW::Settings::GetFSHeight(){
    return GetMonitorHeight(GetCurrentMonitor());
}

int Erik_FW::Settings::GetFSWidth(){
    return GetMonitorWidth(GetCurrentMonitor());
}
