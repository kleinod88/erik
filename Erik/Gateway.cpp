/*
 * Erik The Viking
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Gateway.hpp"
#include "../ErikFW/include/Global_LUA.hpp"
#include "../ErikFW/include/GraphicGameObject.hpp"
#include "Player.hpp"
#include <raylib.h>

Erik::Gateway::Gateway()
    : GraphicGameObject(){
}

void Erik::Gateway::Draw()
{
    GraphicGameObject::Draw();
}

void Erik::Gateway::Clean()
{
    GraphicGameObject::Clean();
}

void Erik::Gateway::Update()
{
    if (Player::Instance()->CollisionDetect(this)) {
        _LUA_Enter();
    }
    _LUA_Update();
}

void Erik::Gateway::ChangeToMap(std::string map){

}




void Erik::Gateway::Load(const std::string scriptFile){
    lua.open_libraries(sol::lib::base);
    lua.open_libraries(sol::lib::string);

    Erik_FW::LuaSetup(&lua);

     std::string erikDir = std::string (getenv("HOME")) + std::string ("/.Erik/");

    lua.set_function("ChangeToMap", &Gateway::ChangeToMap);

    lua.new_usertype<Player>("Player",
        "new", sol::no_constructor,
        "Instance", &Player::Instance,
        "SetPosition", &Player::SetPosition
        );

    lua.script_file (erikDir + scriptFile);

    _ObjectCollision.x = _Position.x + ((_Width - 10)/2);
    _ObjectCollision.y = _Position.y + (_Height-10);
    _ObjectCollision.width = 10;
    _ObjectCollision.height = 10;

    _LUA_Update = lua["Update"];
    _LUA_Draw = lua["Draw"];
    _LUA_Enter = lua["EnterGateway"];

}
