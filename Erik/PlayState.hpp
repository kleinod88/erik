/*
 * Erik Framework
 * Copyright (C) 2022  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef __PLAYSTATE
#define __PLAYSTATE

#include "../ErikFW/include/GameState.hpp"
#include "../ErikFW/include/GameStateFactory.hpp"
#include "../ErikFW/include/GraphicGameObject.hpp"
#include "../ErikFW/include/Map.hpp"
// #include "../Player.hpp"
#include <raylib.h>
#include <vector>

namespace Erik {

class PlayState : public Erik_FW::GameState
{
public:
    PlayState ();

    void HandleEvents() override{};
    virtual void Update() override;
    virtual void Render() override;

    virtual bool OnEnter(std::string file = "") override;
    virtual bool OnExit() override;

    virtual std::string GetStateID() const override;
    bool CheckCollision (Erik_FW::GraphicGameObject *obj1, Erik_FW::GraphicGameObject *obj2);

private:

    static const std::string _PlayID;
    Camera2D _Camera;
    std::vector<Erik_FW::GameObject*> _Objects;

    Music _BackgroundMusic;


    int _CurrentMap;

};

class PlayCreator : public Erik_FW::StateBaseCreator {
public:
    Erik_FW::GameState* CreateState() const {
        return new PlayState();
    }
};

}; //namespace Erik_FW
#endif // PLAYSTATE
