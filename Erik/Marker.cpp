/*
 * Erik Framework
 * Copyright (C) 2022  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Marker.hpp"
#include "Player.hpp"
#include "../ErikFW/include/Game.hpp"
#include "../ErikFW/include/Dialog.hpp"
#include "../ErikFW/include/StoryNode.hpp"
#include "../ErikFW/include/Global_LUA.hpp"
#include <raylib.h>

Erik::Marker::Marker()
    : Erik_FW::GraphicGameObject()
{

}

void Erik::Marker::Draw()
{
    GraphicGameObject::Draw();
    //_LUA_Draw();
}

void Erik::Marker::Clean()
{
    GraphicGameObject::Clean();
}

void Erik::Marker::Update()
{
    if (CheckCollisionRecs(Player::Instance()->GetObject(), _ObjectCollision ) ){

        if (IsGamepadButtonPressed(0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN)||IsKeyPressed(KEY_DOWN)) {
            Erik_FW::Dialog::Instance()->SetMSG(_LUA_GetText());
        }
    }
    _LUA_Update();

}

void Erik::Marker::SetText(std::string text){
    _Text = text;
}



void Erik::Marker::Load(const std::string script){
    lua.open_libraries(sol::lib::base);
    lua.open_libraries(sol::lib::string);


    Erik_FW::LuaSetup(&lua);

    std::string erikDir = std::string (getenv("HOME")) + std::string ("/.Erik/assets/");
    lua.script_file (erikDir + script);


    _ObjectCollision.x = _Position.x;
    _ObjectCollision.y = _Position.y;
    _ObjectCollision.width = _Width;
    _ObjectCollision.height =_Height;

    _LUA_Update = lua["Update"];
    _LUA_Draw = lua["Draw"];
    _LUA_GetText = lua["GetText"];

}
