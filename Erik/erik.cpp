#include "../ErikFW/include/Game.hpp"
#include "../ErikFW/include/GameStateFactory.hpp"
#include "../ErikFW/include/GameStateMaschine.hpp"
#include "../ErikFW/include/GameObjectFactory.hpp"
#include "Menu.hpp"
#include "PlayState.hpp"
#include "Marker.hpp"
#include "Gateway.hpp"
#include "../ErikFW/include/Log.hpp"


int main (){
    Erik_FW::Game::Instance()->Init("Erik der Wikinger");

    Erik_FW::GameStateFactory::Instance()->RegisterType("Menu", new Erik::MenuCreator());
    Erik_FW::GameStateFactory::Instance()->RegisterType("Play", new Erik::PlayCreator());

    Erik_FW::GameObjectFactory::Instance()->RegisterType("Marker", new Erik::MarkerCreator());
    Erik_FW::GameObjectFactory::Instance()->RegisterType("Gateway", new Erik::GatewayCreator());

    Erik_FW::GameStateMaschine::Instance()->ChangeState("Menu", "Main.menu");
    while (Erik_FW::Game::Instance()->Running()){
        Erik_FW::Game::Instance()->HandleEvents();
        Erik_FW::Game::Instance()->Update();
        Erik_FW::Game::Instance()->Render();
    }
    Erik_FW::Game::Instance()->Clean();
    return 0;
}
